<?php

/**
 * @file
 * Contains HFC Private Content module.
 *
 * @ingroup hfcc_modules
 */

/**
 * Set when user is allowed to access private content.
 *
 * @ingroup hfcc_modules
 * @ingroup node_access
 */
define('HFC_PRIVATE_CONTENT_GRANT_ALL', 1);

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Implements hook_node_grants().
 *
 * Tell the node access system what GIDs the user belongs to for each realm.
 *
 * @ingroup hfcc_modules
 * @ingroup node_access
 */
function hfc_private_content_node_grants(AccountInterface $account, $op) {

  $grants = [];

  if ($op == 'view') {
    if ($account->id() != 0) {
      // First grant a grant to the author for own content.
      $grants['private_author'] = [$account->id()];
    }
    if ($account->hasPermission('access private content')) {
      $grants['private_view'] = [HFC_PRIVATE_CONTENT_GRANT_ALL];
    }
  }

  return $grants;
}

/**
 * Implements hook_node_access_records().
 *
 * All node access modules must implement this hook. If the module is
 * interested in the privacy of the node passed in, return a list
 * of node access values for each grant ID we offer.
 *
 * @ingroup hfcc_modules
 * @ingroup node_access
 */
function hfc_private_content_node_access_records(NodeInterface $node) {

  // We only care about the node if its content type is marked private.
  // Otherwise, return nothing.
  if (_hfc_private_content_type($node->bundle())) {
    $grants = [];
    $grants[] = [
      'realm' => 'private_view',
      'gid' => HFC_PRIVATE_CONTENT_GRANT_ALL,
      'grant_view' => 1,
      'grant_update' => 0,
      'grant_delete' => 0,
      'priority' => 0,
    ];

    if ($node->getOwnerId() != 0) {
      $grants[] = [
        'realm' => 'private_author',
        'gid' => $node->getOwnerId(),
        'grant_view' => 1,
        'grant_update' => 0,
        'grant_delete' => 0,
        'priority' => 0,
      ];
    }

    return $grants;
  }
}

/**
 * Implements hook_node_access().
 *
 * @ingroup hfcc_modules
 * @ingroup node_access
 */
function hfc_private_content_node_access(NodeInterface $node, $op, AccountInterface $account) {

  $type = $node->bundle();

  // Apply restrictions on private nodes, except for the owner.
  $owner = ($account->id() != 0) && ($node->getOwnerId() == $account->id());
  if (_hfc_private_content_type($type) && !$owner) {
    if (($op == 'update') || ($op == 'delete')) {
      if (!$account->hasPermission('edit private content')) {
        // Missing access for write.
        return AccessResult::forbidden();
      }
    }
    elseif ($op == 'view') {
      if (!$account->hasPermission('access private content')) {
        // Missing access for view.
        return AccessResult::forbidden();
      }
    }
  }

  // Otherwise, fall back to the pre-existing access rules in core/modules.
  // Note that this module never grants extra access, it only removes it.
  return AccessResult::neutral();
}

/**
 * Checks settings to see if content type is private.
 *
 * @param string $type
 *   Content type to check.
 *
 * @return bool
 *   TRUE if type is private.
 */
function _hfc_private_content_type($type) {
  $types = str_replace(" ", "", \Drupal::config('hfc_private_content.adminsettings')->get('content_types'));
  $private = explode(',', $types);
  return !empty($type) ? in_array($type, $private) : FALSE;
}
