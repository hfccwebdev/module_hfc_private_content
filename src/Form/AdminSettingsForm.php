<?php

namespace Drupal\hfc_private_content\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the HFC Private Content administration form.
 *
 * @package Drupal\hfc_private_content\Form
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('entity_type.manager')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hfc_private_content.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('hfc_private_content.adminsettings');

    // Load current values.
    $current_values = [];
    if (!empty($config->get('content_types'))) {
      foreach (explode(',', $config->get('content_types')) as $key) {
        $current_values[$key] = $key;
      }
    }

    // Load all node content types.
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $options = [];
    foreach ($content_types as $key => $value) {
      $options[$key] = $value->label();
    }

    $form['content_types'] = [
      '#title' => $this->t('Content type(s)'),
      '#type' => 'select',
      '#options' => $options,
      '#multiple' => TRUE,
      '#size' => 15,
      '#description' => $this->t('Select the content types that should be permanently marked as private content.'),
      '#default_value' => $current_values,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $content_types = implode(',', $form_state->getValue('content_types'));

    $this->config('hfc_private_content.adminsettings')
      ->set('content_types', $content_types)
      ->save();

    node_access_needs_rebuild(TRUE);
  }

}
